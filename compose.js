module.exports = function compose(fns) {
    console.log("I'm compose");
    const inverseFns = fns.reduce((currentArray, elem) => {
           return [elem, ...currentArray];
    }, []);
   
    return function () {
        return inverseFns.reduce((res, currentElem) => {
            console.log(res);
            if(typeof res == 'object' ) return currentElem(...res);
            return currentElem(res);                    
        }, [...arguments]);
    };
};