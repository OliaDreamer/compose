const tape = require('tape');

const compose = require('./compose');

tape.test("Run compose", function (t) {
    let fns = [x => x - 8, x => x ** 2, (x, y) => (y > 0) ? (x + 3) : (x - 3)];

    // (x, y) => (x - 2) * (y - 2) -> 5
    // 5 => 5 * 2 -> 10 
    // x => x + 1 -> 11
    
    const fn = compose(fns);

    t.same(typeof fn, 'function');

    t.same(fn('3', 1), 1081);

    // t.same(1, 1);

    t.end();
});